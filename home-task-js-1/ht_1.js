// let name = prompt('Name');
// let age = prompt('age', 18);
//
// if (age < 18) {
//     alert('You are not allowed to visit this website.');
// } else if (age <= 22) {
//     let ok_cancel = confirm('Are you sure you want to continue?');
//     if (ok_cancel) {
//         alert(`Welcome ${name}`);
//     }else{
//       alert('You are not allowed to visit this website.');
//     }
//
// } else {
//     alert(`Welcome ${name}`);
// }

let name = "";
let age = 18;

do {
    name = prompt('Enter your name', name);
    age = parseInt(prompt('Enter your age', age));
} while (name === "" || Number.isNaN(age) || age < 0);

if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age <= 22) {
    let ok_cancel = confirm('Are you sure you want to continue?');
    if (ok_cancel) {
        alert(`Welcome ${name}`);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert(`Welcome ${name}`);
}

